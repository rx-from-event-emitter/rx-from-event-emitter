import * as Rx from '@reactivex/rxjs'

export function makeRxObservableFromEventEmitter<Output>
    (emitter: any, nextEvents: string[], errorEvents: string[], completeEvents: string[]): Rx.Observable<Output> {
    return Rx.Observable.create((subscriber: Rx.Subscriber<Output>) => {
        for (let nextEvent of nextEvents)
            emitter.on(nextEvent, (data: Output) => subscriber.next(data))
        for (let errorEvent of errorEvents)
            emitter.on(errorEvent, (err: any) => subscriber.error(err))
        for (let completeEvent of completeEvents)
            emitter.on(completeEvent, () => subscriber.complete())
    })
}

export function makeRxObservableFromEventEmitterArray<Output>
    (emitter: any, nextEvents: string[], errorEvents: string[], completeEvents: string[]): Rx.Observable<Output[]> {
    return Rx.Observable.create((subscriber: Rx.Subscriber<Output[]>) => {
        for (let nextEvent of nextEvents)
            emitter.on(nextEvent, (...data: Output[]) => subscriber.next(data))
        for (let errorEvent of errorEvents)
            emitter.on(errorEvent, (err: any) => subscriber.error(err))
        for (let completeEvent of completeEvents)
            emitter.on(completeEvent, () => subscriber.complete())
    })
}
