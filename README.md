# Description

This module extends the capabilities offered by Rx.Observable.fromEvent(), by allowing multiple kinds of events to be handled in one of the three ways a subscription allows.

# Usage

Example: suppose that `message` is of type `http.IncomingMessage`; you can use several kinds of events from it and they will be treated in one of the three ways corresponding to the subscription mechanism:
  * ['data']: put here the normal events, and their outputs will be given to `next()`;
  * ['error']: put here the error events, and their outputs will be given to `error()`;
  * ['close', 'end']: put here the events corresponding to `complete()`.

The events arrays must be given in that order: next, error, complete.

So you can write:
`makeRxObservableFromEventEmitter<Buffer>(message, ['data'], ['error'], ['close', 'end'])`

In some cases, the normal output is an Array, such as `Buffer[]`. In these cases, use this function:
`makeRxObservableFromEventEmitterArray<Buffer>(emitter, ['data'], ['error'], ['close', 'end'])`
